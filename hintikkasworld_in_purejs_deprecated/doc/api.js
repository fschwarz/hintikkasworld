YUI.add("yuidoc-meta", function(Y) {
   Y.YUIDoc = { meta: {
    "classes": [
        "ActionModel",
        "EpistemicModel",
        "Graph",
        "TrivialPostCondition",
        "Valuation",
        "createFormula",
        "createLitteral",
        "createWorldActionName",
        "formulaPrettyPrint",
        "product"
    ],
    "modules": [],
    "allModules": [],
    "elements": []
} };
});