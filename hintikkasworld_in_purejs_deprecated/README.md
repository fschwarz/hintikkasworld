Welcome to Hintikka's world! This website shows intelligent artificial agents reasoning about higher-order knowledge (a knows that b knows that...). It enables to explore mental states of the agents by clicking on them. It contains many classical AI examples. It is a tribute to Jaakko Hintikka. This tool can be used for:

    learning modal logic, model checking and satisfiability problem;
    learning models of dynamic epistemic logic;
    having fun with epistemic puzzles.


The tool is available at http://hintikkasworld.irisa.fr/.